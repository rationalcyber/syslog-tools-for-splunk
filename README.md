# Syslog Tools for Splunk

Our syslog monitoring, auto-sourcetyping, auto-timezoning app for Splunk.

# Installation Instructions

Install the app to your Splunk search head. Edit the values of the following macros:
1) syslog_servers - The value of this macro should be a comma-separated list of 
your syslog server hostnames in parentheses, such as “(syslog1, syslog2, 
syslog3)”. The app defaults to a search that will correctly autofill the correct
values in environments that use our recommended rsyslog and syslog-ng 
configurations, but entering the values manually will result in better 
performance and reliability. Wildcards (e.g., “(syslogserver*)” are acceptable.

2) syslog_source_pattern - The values of this macro should be a pattern or 
patterns of values of the Splunk source field that can reliably identify syslog 
data sources that were sent to Splunk by a UF or HF monitoring files on a syslog 
server. It should take the form “source IN (/log/directory/name/\*)”. It does not 
necessarily need to rely on the source field, however; any indexed field that 
can reliably differentiate syslog sources in your environment is acceptable. For 
example, if you create an indexed field for syslog_server, “syslog_server=\*” 
would be acceptable (assuming you properly specified it in fields.conf).